from django import forms
from AppMH.models import Persona
from datetime import datetime

class formPersona(forms.Form):
    nombre_completo = forms.CharField(label='Nombre:', max_length=100, 
                                      widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Ej: Antonio Varas',}))

    rut = forms.CharField(label='RUT:', max_length = 10, 
                          widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Ej: 12345678-9',}))

    fecha_nac = forms.DateField(label='Fecha de nacimiento:', 
                                widget=forms.SelectDateWidget(years=range(1900,2001), attrs={'class':'form-control',}))

    telefono = forms.CharField(label='Telefono:', max_length = 9,
                                  widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Ej: 912345678',}))

    email = forms.EmailField(label='Email:', max_length=100, 
                             widget=forms.EmailInput(attrs={'class':'form-control', 'placeholder':'Ej: usuario@empresa.com',}))


    def agregar_persona(self):
            fecha = datetime(int(self.data['fecha_nac_year']),
                            int(self.data['fecha_nac_month']),
                            int(self.data['fecha_nac_day']))
            
            new_persona = Persona(nombre_completo=self.data['nombre_completo'],
                            rut=self.data['rut'],
                            fecha_nac=fecha,
                            telefono=self.data['telefono'],
                            email=self.data['email'])
            new_persona.save()
            return 'Se han registrado sus datos'