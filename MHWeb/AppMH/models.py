from django.db import models
from django.utils.translation import ugettext as _ 

# Create your models here.

class Persona(models.Model):
    nombre_completo = models.CharField(max_length = 100, 
        help_text='p. ej. Juan Perez')

    rut = models.CharField(max_length = 10, 
        help_text='p. ej. 12654789-5')

    fecha_nac = models.DateField()

    telefono = models.CharField(max_length = 9,help_text='p. ej. 987654321')

    email = models.EmailField(max_length=100, help_text='p. usuario@empresa.com')

    def __str__(self):
        return self.rut

    class Meta:
        permissions = (
            ('Gremio',_('Pertenece al gremio')),
            ('Usuario',_('Usuario comun de la pagina')),
        )