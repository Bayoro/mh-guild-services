/**
 * Este archivo cuenta con validaciones para el formulario de postulación
 * a recorridos turísticos.
 */

$(document).ready(function(){
	$("#formulario1").submit(function(evento){
		if(!valido()){
			alert("Datos incorrectos o faltantes: No se pudo enviar el formulario");
			evento.preventDefault();
		}
	});
	$("[name='Nombre']").blur(validarNombre);
    $("[name='Fecha']").blur(validarFecha);
    //$("[name='RUT']").blur(validarRUT);
    $("[name='Telefono']").blur(validarFono);
});

function valido(){
	//Validar que los campos no estén vacíos
	var valido = true;
	valido = validarNombre() && valido;
	valido = validarFecha() && valido;
    //valido = validarRUT() && valido;
    valido = validarFono() && valido;
	return valido;
}

function validarNombre() {
    //Validar nombre correcto
	$("#error_nombre").html("");
	var nombre = $("[name='Nombre']").val();
	nombre = $.trim(nombre);
	if(nombre.length == 0){
		$("#error_nombre").html("Nombre en blanco");
		return false;
	}
	return true;
}

function validarFecha() {
    //Validar ciertos rangos de fecha
	$("#error_fecha").html("");
	var fecha_nac_string = $("[name='Fecha']").val();
	var fecha_nac = new Date(fecha_nac_string);
	var hoy = new Date();
	if(fecha_nac > hoy){
		$("#error_fecha").html("Por favor, ingresa una fecha válida");
		return false;
	}
	if(fecha_nac.getFullYear() < 1900){		
		$("#error_fecha").html("Por favor, ingresa una fecha de nacimiento real.");
		return false;
	}
	return true;
}

function validarFono(){
    //Validar si el telefono ingresado es número longitud del telefono ingresado
    $("#error_telefono").html("");
    var fono = $("[name='Telefono']").val();
    if(isNaN(fono)){
        $("#error_telefono").html("Nro incorrecto, no introduzca letras");
        return false;
    }
    if(fono.length != 9){
		$("#error_telefono").html("Nro incorrecto, un teléfono contiene 9 dígitos");
		return false;
	}
	return true;
}