from django.urls import include, path
from . import views

urlpatterns = [
    path('', views.index , name='index'),
    path('gremio/', views.gremio , name='gremio'),
    path('felyne/', views.felyne , name='felyne'),
    path('cazadores/', views.cazadores , name='cazadores'),
    path('postular/', views.postular , name='postular'),
]