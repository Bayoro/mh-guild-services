from django.shortcuts import render, redirect
from .forms import formPersona
from .models import Persona

# Create your views here.

def index(request):
    usuario = request.user
    if usuario.has_perm('AppMH.Gremio'):
        return render(request, 'CRUDPostulantes/Main.html')
    else:
        return render(request, 'AppMH/index.html')

def gremio(request):
    return render(request, 'AppMH/Gremio.html')

def felyne(request):
    return render(request, 'AppMH/felyne.html')

def cazadores(request):
    return render(request, 'AppMH/cazadores.html')

def postular(request):
    if request.method == 'POST':
        add_persona = formPersona(request.POST)
        if add_persona.is_valid():
            success = add_persona.agregar_persona()
            return redirect('./')
    else:
        add_persona = formPersona()
        return render(request, 'AppMH/Formulario.html', {'add_persona': add_persona})