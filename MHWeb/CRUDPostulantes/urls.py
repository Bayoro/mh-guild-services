from django.urls import include, path
from . import views

urlpatterns = [
    path('', views.crud , name='crud'),
    path('crear/', views.crear , name='crear'),
    path('listar/', views.listar , name='listar'),
    path('editar/', views.editar , name='editar'),
    path('eliminar/', views.eliminar , name='eliminar'),
]