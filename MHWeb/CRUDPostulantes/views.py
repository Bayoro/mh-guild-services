from django.shortcuts import render, redirect
from .forms import formPersona
from AppMH.models import Persona

# Create your views here.

def crud(request):
    return render(request, 'CRUDPostulantes/Main.html')

def crear(request):
    if request.method == 'POST':
        add_persona = formPersona(request.POST)
        if add_persona.is_valid():
            success = add_persona.agregar_persona()
            return redirect('CRUDPostulantes/Listar.html')
    else:
        add_persona = formPersona()
        return render(request, 'CRUDPostulantes/Crear.html', {'add_persona': add_persona})

def listar(request):
    Postulantes = Persona.objects.all()
    return render(request, 'CRUDPostulantes/Listar.html', {'postulantes': Postulantes})

def editar(request):
    return render(request, 'CRUDPostulantes/Editar.html')

def eliminar(request):
    return render(request, 'CRUDPostulantes/Eliminar.html')